<?php

use yii\db\Migration;

/**
 * Class m200308_074431_add_shop_product_status_field
 */
class m200308_074431_add_shop_product_status_field extends Migration
{
    public function up()
    {
        $this->addColumn('{{%shop_products}}', 'status', $this->smallInteger()->defaultValue(1));

    }

    public function down()
    {
        $this->dropColumn('{{%shop_products}}', 'status');
    }
}
