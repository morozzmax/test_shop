<?php

use yii\db\Migration;

/**
 * Class m200301_101305_rename_user_table
 */
class m200301_101305_rename_user_table extends Migration
{
    public function up()
    {
        $this->renameTable('{{%user}}', '{{%users}}');
    }

    public function down()
    {
        $this->renameTable('{{%users}}', '{{%user}}');
    }
}
