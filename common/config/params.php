<?php
return [
    'adminEmail' => 'admin@example.com',
    'frontendHostInfo' => 'http://localhost:8080',
    'backendHostInfo' => 'http://localhost:8081',
    'user.passwordResetTokenExpire' => 3600,
    'user.rememberMeDuration' => 3600 * 24 * 30,
    'staticHostInfo' => 'http://localhost:8087',
    'staticPath' => dirname(__DIR__, 2) . '/static',
];
