<?php
namespace common\bootstrap;

use frontend\urls\CategoryUrlRule;
use shop\readModels\Shop\CategoryReadRepository;
use shop\services\ContactService;
use yii\base\BootstrapInterface;
use yii\caching\Cache;
use yii\di\Instance;
use yii\mail\MailerInterface;
use Elasticsearch\Client;
use Elasticsearch\ClientBuilder;

class SetUp implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = \Yii::$container;

        $container->setSingleton(MailerInterface::class, function () use ($app) {
            return $app->mailer;
        });

        $container->setSingleton(ContactService::class, [], [
            $app->params['adminEmail']
        ]);

        $container->setSingleton(Cache::class, function () use ($app) {
            return $app->cache;
        });

        $container->setSingleton(Client::class, function () {
            return ClientBuilder::create()->setHosts(["shop-elasticsearch:9200"])->build();
        });

        $container->set(CategoryUrlRule::class,[],[
            Instance::of(CategoryReadRepository::class),
            Instance::of(Cache::class)
        ]);
    }
}